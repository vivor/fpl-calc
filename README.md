FPL Calculator
==============

This is a simple tool to look up the US Federal Poverty Level for a given state and household size.

To use it, construct an instance of the class with an object containing the current Federal Poverty Level
threshholds for each region. The format is as follows, using the 2020 levels as an example:

```
const FplCalc = require('fpl-calc')

const fplCalculator = new FplCalc({
  "default": {
    "levels": [12759, 17240, 21720, 26200, 30680, 35160, 39640, 44120],
    "additional": 4479
  },
  "HI": {
    "levels": [14679, 19830, 24980, 30130, 35280, 40430, 45580, 50730],
    "additional": 5149
  },
  "AK": {
    "levels": [15949, 21550, 27150, 32750, 38350, 43950, 49550, 55150],
    "additional": 5599
  }
})
```

The `levels` key is a list of income thresholds for each household size, with levels above the provided
being found by adding `additional` for each additional household member.

There are two methods, `.level(state, householdSize)` to look up given state and household size, and `.percentage(state, householdSize, householdIncome)` which calculates
the percentage of FPL that a given income represents.

```
> fplCalculator.level('OR', 2)
17240
> fplCalculator.level('HI', 10)
61028
> fplCalculator.percentage('AK', 1, 50000)
313
```
