'use strict'

/**
Calculates FPL for a given state and household size

@constructor
@param {Object} config Income levels with state-level overrides, formatted as follows:
```
{
  default: {
    levels: [1000, 2000, 3000],
    additional: 1000,
  },
  HI: {
    levels: [2000, 3000, 4000],
    additional: 1500,
  }
}
```
*/
function FplCalculator (config) {
  this.config = config
}

FplCalculator.prototype.level = function (state, household_size) {
  if (household_size == null || household_size <= 0) { return null }

  var values = (state in this.config) ? this.config[state] : this.config.default
  if (household_size > values.levels.length) {
    return values.levels[values.levels.length - 1] + values.additional * (household_size - values.levels.length)
  } else {
    return values.levels[household_size - 1]
  }
}

FplCalculator.prototype.percentage = function (state, household_size, household_income) {
  if (!household_size) { return null }
  if ((!household_income && household_income !== 0) || household_income < 0) { return null }

  return Math.round(household_income / this.level(state, household_size) * 100)
}

module.exports = FplCalculator
