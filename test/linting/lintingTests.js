'use strict'
const AppRoot = require('app-root-path')

const lint = require('mocha-eslint')
const paths = [`${AppRoot}/*.js`, `${AppRoot}/src/**/*.js`, `${AppRoot}/test/**/*.js`]
const options = {
  strict: true,
  contextName: 'mocha'
}
// Run the tests
describe('linting tests', function () {
  lint(paths, options)
})
