'use strict'
const should = require('chai').should()
const AppRoot = require('app-root-path')
const FplCalculator = require(`${AppRoot}/src/fplCalculator`)

let fpl
beforeEach(function () {
  fpl = new FplCalculator({
    default: {
      levels: [15, 30, 40],
      additional: 5
    },
    HI: {
      levels: [10, 20, 30, 40, 50],
      additional: 20
    }
  })
})

describe('calculateFpl levels', function () {
  it('should look up base levels directly', function () {
    fpl.level('WA', 1).should.equal(15)
    fpl.level('WA', 2).should.equal(30)
    fpl.level('WA', 3).should.equal(40)
  })
  it('should calculate for additional members', function () {
    fpl.level('WA', 4).should.equal(45)
    fpl.level('WA', 5).should.equal(50)
    fpl.level('WA', 6).should.equal(55)
  })
  it('should use overrides if they exist', function () {
    fpl.level('HI', 1).should.equal(10)
    fpl.level('HI', 2).should.equal(20)
    fpl.level('HI', 3).should.equal(30)
    fpl.level('HI', 4).should.equal(40)
    fpl.level('HI', 5).should.equal(50)
    fpl.level('HI', 6).should.equal(70)
    fpl.level('HI', 7).should.equal(90)
  })
  it('should handle invalid values', function () {
    should.not.exist(fpl.level('OR', -10))
    should.not.exist(fpl.level('OR', 0))
    should.not.exist(fpl.level('OR', null))
  })
})

describe('calculateFpl percentages', function () {
  it('should calculate percentage of household income', function () {
    fpl.percentage('HI', 2, 40).should.equal(200)
    fpl.percentage('WA', 5, 49.9).should.equal(100)
    fpl.percentage('WA', 5, 49.5).should.equal(99)
    fpl.percentage('WA', 25, 0).should.equal(0)
  })
  it('should handle invalid values', function () {
    should.not.exist(fpl.percentage('OR', 1, -5))
    should.not.exist(fpl.percentage('OR', 1, null))
    should.not.exist(fpl.percentage('OR', null, 5))
    should.not.exist(fpl.percentage('OR', 2, -5))
    should.not.exist(fpl.percentage('OR', '', -5))
    should.not.exist(fpl.percentage('OR', 2, ''))
  })
})
